import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// This is used only when reassigning variables
import { Observable, BehaviorSubject } from 'rxjs';

import {
  IPokemon,
  IPokemonApiResponse,
  ITypesApiResponse
} from './../interfaces/pokemon.interface';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private baseUrl: string;
  public pokemonList: IPokemon[];

  // This variable will change it's assigned value so we have to use an Observable
  private _currentIdSubject = new BehaviorSubject(1);
  public currentId: Observable<number>;

  constructor(private http: HttpClient) {
    this.baseUrl = 'https://pokeapi.co/api/v2/pokemon/';
    this.pokemonList = [];
    this.currentId = this._currentIdSubject.asObservable();
  }

  public getPokemonList(): IPokemon[] {
    return this.pokemonList;
  }

  public getPokemonUrl(id: number): string {
    return `${this.baseUrl}${id}/`;
  }

  public fetchPokemon(id: number) {
    const url = this.getPokemonUrl(id);

    this.http.get(url).subscribe((response: IPokemonApiResponse) => {
      // Format the pokemon response to fit our data structure and interfaces
      const newPokemon = {
        id: response.id,
        name: response.name,
        image: response.sprites.front_default,
        types: response.types.map((apiType: ITypesApiResponse) => apiType.type.name)
      };

      this.pokemonList.push(newPokemon);
    });
  }

  public setIncreaseCurrentId(value: number) {
    // Upate the value of currentId using it's subject
    this._currentIdSubject.next(value);
    this.fetchPokemon(value);
  }
}
